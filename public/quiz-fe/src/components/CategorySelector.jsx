import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useState } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import IconButton from "@material-ui/core/IconButton";
import ManagerLiterature from "./ManagarLiterature";
import ManagerGeography from "./ManagerGeography";
import ManagerHistory from "./ManagerHistory";
import ManagerOther from "./ManagerOther";
import { Slide } from "react-awesome-reveal";


const useStyles = makeStyles({
  root: {
    maxWidth: 345,
    minWidth: 300,
    margin: "1rem",
    display: "flex",
    minHeight:400
  },
  media: {
    height: 140,
  },
});

export default function CategorySelector(props) {
  const classes = useStyles();

  const [literature, setLiterature] = useState(false);

  const [history, setHistory] = useState(false);

  const [geography, setGeography] = useState(false);

  const [other, setOther] = useState(false);

  return (
    <div>
        <Router>
        <Slide left>
          <div style={{
            display: "flex",
            justifyContent: "space-around",
            flexWrap: "wrap",
          }}  >
          <Card className={classes.root}>
            <CardActionArea>
              <CardMedia
              style={{
                marginTop:"15px"
              }}
                component="img"
                alt="Contemplative Reptile"
                height="170"
                image="/imgs/hi.jpg"
                title="Hi"
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  History
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                  Here you can select the history category to add the quiz question.
                </Typography>
              </CardContent>
              <Link
          to="history"
          style={{textDecoration: "none"}}>
          <IconButton onClick={() => setHistory(true)}>
                <AddCircleIcon fontSize="large" />
              </IconButton>
              </Link>
            </CardActionArea>
          </Card>
          <Card className={classes.root}>
            <CardActionArea>
              <CardMedia
                component="img"
                alt="Contemplative Reptile"
                height="170"
                image="/imgs/geo.jpg"
                title="Contemplative Reptile"
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  Geography
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                  Here you can select the geography category to add the quiz question.
                </Typography>
              </CardContent>
              <Link
          to="geography"
          style={{textDecoration: "none"}}>
              <IconButton onClick={() => setGeography(true)}>
                <AddCircleIcon fontSize="large" />
              </IconButton>
              </Link>
            </CardActionArea>
          </Card>
          <Card className={classes.root}>
            <CardActionArea>
              <CardMedia
                component="img"
                alt="Contemplative Reptile"
                height="170"
                image="/imgs/li.jpg"
                title="Contemplative Reptile"
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  Literature
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                  Here you can select the literature category to add the quiz question.
                </Typography>
              </CardContent>
              <Link
          to="literature"
          style={{textDecoration: "none"}}>
              <IconButton onClick={() => setLiterature(true)}>
                <AddCircleIcon fontSize="large" />
              </IconButton>
              </Link>
            </CardActionArea>
          </Card>
          <Card className={classes.root}>
            <CardActionArea>
              <CardMedia
                component="img"
                alt="Contemplative Reptile"
                height="170"
                image="/imgs/other.jpg"
                title="Contemplative Reptile"
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  Other
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                  Here you can select the other category to add the quiz question.
                </Typography>
              </CardContent>
              <Link
          to="other"
          style={{textDecoration: "none"}}>
              <IconButton onClick={() => setOther(true)}>
                <AddCircleIcon fontSize="large" />
              </IconButton>
              </Link>
            </CardActionArea>
          </Card>
        </div>

        </Slide>
        <Switch>
        <Route path="/history">
          {" "}
          <ManagerHistory data={props.data} />{" "}
        </Route>
        <Route path="/geography">
          {" "}
          <ManagerGeography data={props.data} />{" "}
        </Route>
        <Route path="/literature">
          {" "}
          <ManagerLiterature data={props.data} />{" "}
        </Route>
        <Route path="/other">
          <ManagerOther data={props.data} />
        </Route>
      </Switch>
        </Router>
      
    </div>
  );
}
