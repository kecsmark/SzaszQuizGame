import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import SaveIcon from "@material-ui/icons/Save";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { useState } from "react";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { Slide } from "react-awesome-reveal";
import li from "../pngs/li.png";


export default function ManagerLiterature(props) {

  const [question, setQuestion] = useState("");
  const [answer1, setAnswer1] = useState("");
  const [answer2, setAnswer2] = useState("");
  const [answer3, setAnswer3] = useState("");
  const [answer4, setAnswer4] = useState("");

  const [correct1, setCorrect1] = useState(false);
  const [correct2, setCorrect2] = useState(false);
  const [correct3, setCorrect3] = useState(false);
  const [correct4, setCorrect4] = useState(false);
  const [correctAnswer, setCorrectAnswer] = useState("");

  const handleQuestion = (event) => {
    setQuestion(event.target.value);
  };

  const handleAnswer1 = (event) => {
    setAnswer1(event.target.value);
  };
  const handleAnswer2 = (event) => {
    setAnswer2(event.target.value);
  };
  const handleAnswer3 = (event) => {
    setAnswer3(event.target.value);
  };

  const handleAnswer4 = (event) => {
    setAnswer4(event.target.value);
  };

  const handleCorrect1 = (event) => {
    setCorrect1(event.target.checked);
    setCorrectAnswer(answer1);
    console.log(correctAnswer)
  };

  const handleCorrect2 = (event) => {
    setCorrect2(event.target.checked);
    setCorrectAnswer(answer2);
    console.log(correctAnswer)
  };

  const handleCorrect3 = (event) => {
    setCorrect3(event.target.checked);
    setCorrectAnswer(answer3);
    console.log(correctAnswer)
  };

  const handleCorrect4 = (event) => {
    setCorrect4(event.target.checked);
    setCorrectAnswer(answer4);
    console.log(correctAnswer)
  };

  let id;

  const handleAddquiz = async () => {
    
    props.data.map((user) => (id = user.id));
    const response = await fetch("http://localhost:4000/quiz/createLiterature", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        Question: question,
        Answer1: answer1,
        Answer2: answer2,
        Answer3: answer3,
        Answer4: answer4,
        Correct: correctAnswer,
        UserID: id,
        Valid: false,
      }),
    });
    if (response.ok) {
      const data = await response.json();
      window.alert("Quiz saved")
      window.open("http://localhost:3000/quizmanager", "_self")
      console.log(data);
    }
  };

  return (
    <div>
        <Slide left>
          <div>
            <img style={{
              maxWidth:"70%",
            }} src={li} alt="li"/>
          </div>
        <Typography variant="body2" gutterBottom>
        This is the literature category, here you can create questions that are related to literature.
      </Typography>
      <Form
        style={{
          margin: "10px",
        }}
      >
        <Form.Group
          controlId="formGroupEmail"
          style={{
            margin: "5px",
          }}
        >
          <Form.Label>Question</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter question"
            onChange={handleQuestion}
          />
        </Form.Group>
        <div
          style={{
            margin: "5px",
          }}
        >
          <Row>
            <Col>
              <Form.Label>Answer1</Form.Label>
              <Form.Control placeholder="Answer" onChange={handleAnswer1} />
            </Col>
            <Col>
              <Form.Label>Answer2</Form.Label>
              <Form.Control placeholder="Answer" onChange={handleAnswer2} />
            </Col>
          </Row>
        </div>
        <div
          style={{
            margin: "5px",
          }}
        >
          <Row>
            <Col>
              <Form.Label>Answer3</Form.Label>
              <Form.Control placeholder="Answer" onChange={handleAnswer3} />
            </Col>
            <Col>
              <Form.Label>Answer4</Form.Label>
              <Form.Control placeholder="Answer" onChange={handleAnswer4} />
            </Col>
          </Row>
        </div>
        {correct2 || correct3 || correct4 ? (
         <FormControlLabel
         control={
           <Checkbox
             disabled
             name="checkedB"
             color="primary"
             onChange={handleCorrect1}
           />
         }
         label="1"
       />
        ) : (
            <FormControlLabel
            control={
              <Checkbox
                name="checkedB"
                color="primary"
                onChange={handleCorrect1}
              />
            }
            label="1"
          />
        )}
        {correct1 || correct3 || correct4 ? (
          <FormControlLabel
            control={
              <Checkbox
                disabled
                name="checkedB"
                color="primary"
                onChange={handleCorrect2}
              />
            }
            label="2"
          />
        ) : (
          <FormControlLabel
            control={
              <Checkbox
                name="checkedB"
                color="primary"
                onChange={handleCorrect2}
              />
            }
            label="2"
          />
        )}
        {correct1 || correct2 || correct4 ? (
          <FormControlLabel
            control={
              <Checkbox
                disabled
                name="checkedB"
                color="primary"
                onChange={handleCorrect3}
              />
            }
            label="3"
          />
        ) : (
          <FormControlLabel
            control={
              <Checkbox
                name="checkedB"
                color="primary"
                onChange={handleCorrect3}
              />
            }
            label="3"
          />
        )}
        {correct1 || correct2 || correct3 ? (
          <FormControlLabel
            control={
              <Checkbox
                disabled
                name="checkedB"
                color="primary"
                onChange={handleCorrect4}
              />
            }
            label="4"
          />
        ) : (
          <FormControlLabel
            control={
              <Checkbox
                name="checkedB"
                color="primary"
                onChange={handleCorrect4}
              />
            }
            label="4"
          />
        )}
      </Form>
      {question.length >= !0 &&
      answer1.length >= !0 &&
      answer2.length >= !0 &&
      answer3.length >= !0 &&
      answer4.length >= !0 &&
      (correct1 || correct2 || correct3 || correct4)&&correctAnswer!=null&&correctAnswer.length!==0? (
        <Button onClick={handleAddquiz}  variant="outlined" >
          <SaveIcon /> Save
        </Button>
      ) : (
        <Button disabled  variant="outlined">
          <SaveIcon /> Save
        </Button>
      )}
        </Slide>      
    </div>
  );
}