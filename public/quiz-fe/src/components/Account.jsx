import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import { Alert, AlertTitle } from "@material-ui/lab";
import { CircularProgressbar } from "react-circular-progressbar";
import { useDispatch, useSelector } from "react-redux";
import { deleteUser } from "../store/loginSlice";
import { Slide } from "react-awesome-reveal";
import { initScore, selectScore } from "../store/scoreSlice";
import {  useEffect } from "react";
import "react-circular-progressbar/dist/styles.css";
import PublicIcon from "@material-ui/icons/Public";
import AccountBalanceIcon from "@material-ui/icons/AccountBalance";
import BookIcon from "@material-ui/icons/Book";
import DynamicFeedIcon from "@material-ui/icons/DynamicFeed";
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import PermIdentityIcon from '@material-ui/icons/PermIdentity';

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 360,
    minWidth: 360,
    margin: "1rem",
    minHeight:400,
  },
  media: {
    height: 0,
    paddingTop: "56.25%", // 16:9
  },
  avatar: {
    backgroundColor: '#009b9b',
  },
  label: {
    margin: 20,
    padding: 10,
  }
}));

export default function Account(props) {
  const dispatch = useDispatch();
  const classes = useStyles();
  const scoreStore = useSelector(selectScore);

  const handleScore = async (id) => {
    const response = await fetch(`http://localhost:4000/quiz/userScore/${id}`, {
      method: "GET",
    });
    const data = await response.json();
    if (response.ok) {
      dispatch(initScore([data]));
    }
  };

  const handleClick = async (id) => {
    const response = await fetch(
      `http://localhost:4000/quiz/deleteUser/${id}`,
      {
        method: "DELETE",
      }
    );
    const data = await response.json();
    if (response.ok) {
      window.alert(data);
      dispatch(deleteUser(id));
      localStorage.clear();
      window.location.reload();
    }
  };
  let id = props.data.map((user) => {
    return user.id;
  });
  useEffect(() => {
    setTimeout(() => {
      handleScore(id);
    }, 1000);
  }, []);

  return (
    <div>
      <Slide left>
        <div style={{
            display: "flex",
            justifyContent: "space-between",
            flexWrap: "wrap",
          }}>
          <Card className={classes.root} >
            <CardHeader
              avatar={
                <Avatar aria-label="recipe" className={classes.avatar}>
                  <AccountBalanceIcon />
                </Avatar>
              }
            />
            <div
              style={{
                width: 200,
                height: 200,
                pathTransitionDuration: 0.5,
                margin: "0 auto",
              }}
            >
              <CircularProgressbar
                value={scoreStore.map((user) => {
                  return user.ScoreHistory;
                })}
                text={scoreStore.map((user) => {
                  return user.ScoreHistory;
                })}
                maxValue={1000}
              />
            </div>
            <CardContent>
              <Typography variant="body2" color="textSecondary" component="p" className={classes.label}>
                History Score
              </Typography>
            </CardContent>
          </Card>

          <Card className={classes.root}>
            <CardHeader
              avatar={
                <Avatar aria-label="recipe" className={classes.avatar}>
                  <PublicIcon />
                </Avatar>
              }
            />
            <div
              style={{
                width: 200,
                height: 200,
                pathTransitionDuration: 0.5,
                margin: "0 auto",
              }}
            >
              <CircularProgressbar
                value={scoreStore.map((user) => {
                  return user.ScoreGeography;
                })}
                text={scoreStore.map((user) => {
                  return user.ScoreGeography;
                })}
                maxValue={1000}
              />
            </div>
            <CardContent>
              <Typography variant="body2" color="textSecondary" component="p" className={classes.label}>
                Geography Score
              </Typography>
            </CardContent>
          </Card>

          <Card className={classes.root}>
            <CardHeader
              avatar={
                <Avatar aria-label="recipe" className={classes.avatar}>
                  <BookIcon />
                </Avatar>
              }
            />
            <div
              style={{
                width: 200,
                height: 200,
                pathTransitionDuration: 0.5,
                margin: "0 auto",
              }}
            >
              <CircularProgressbar
                value={scoreStore.map((user) => {
                  return user.ScoreLiterature;
                })}
                text={scoreStore.map((user) => {
                  return user.ScoreLiterature;
                })}
                maxValue={1000}
              />
            </div>
            <CardContent>
              <Typography variant="body2" color="textSecondary" component="p" className={classes.label}>
                Literature Score
              </Typography>
            </CardContent>
          </Card>

          <Card className={classes.root}>
            <CardHeader
              avatar={
                <Avatar aria-label="recipe" className={classes.avatar}>
                  <DynamicFeedIcon />
                </Avatar>
              }
            />
            <div
              style={{
                width: 200,
                height: 200,
                pathTransitionDuration: 0.5,
                margin: "0 auto",
              }}
            >
              <CircularProgressbar
                value={scoreStore.map((user) => {
                  return user.ScoreOther;
                })}
                text={scoreStore.map((user) => {
                  return user.ScoreOther;
                })}
                maxValue={1000}
              />
            </div>
            <CardContent>
              <Typography variant="body2" color="textSecondary" component="p" className={classes.label}>
                Other Score
              </Typography>
            </CardContent>
          </Card>
        </div>
        <div>
        <Card style={{
          margin:"0 auto",
          marginTop:"50px"
        }}>
          <CardHeader
            avatar={
              <Avatar aria-label="recipe" className={classes.avatar}>
                <PermIdentityIcon />
              </Avatar>
            }
          />
          <div>
            <h1>{props.data.map((user) => {
                return user.Username;
              })}</h1>

          </div>
          <CardContent>
              <Typography variant="body2" color="textSecondary" component="p" className={classes.label}>
                Name
              </Typography>
          </CardContent>
        </Card>
        <Card style={{
          margin:"0 auto",
          marginTop:"30px"
        }} >
          <CardHeader
            avatar={
              <Avatar aria-label="recipe" className={classes.avatar}>
                <MailOutlineIcon />
              </Avatar>
            }
          />
          <div>
            <h1>
            {props.data.map((user) => {
                return user.Email;
              })}
            </h1>
          </div>
          <CardContent>
              <Typography variant="body2" color="textSecondary" component="p" className={classes.label}>
                E-mail adress
              </Typography>
          </CardContent>
        </Card>
  
</div>
        <div
          style={{
            marginTop: "100px",
            
          }}
        >
          <div
            style={{
              border: "1px solid red",
              margin: "0 auto",
              width: "700px",
              height: "300px",
            }}
          >
            <legend>Delete your account</legend>
            <h4>To delete your account, click the trash can icon.</h4>
            <IconButton
              aria-label="delete"
              className={classes.button}
              onClick={()=>handleClick(id)}
            >
              <DeleteIcon fontSize="large" />
            </IconButton>
            <Alert severity="warning">
              <AlertTitle>Warning</AlertTitle>
              <strong>You will lose all data!</strong>
            </Alert>
          </div>
        </div>
      </Slide>
    </div>
  );
}
