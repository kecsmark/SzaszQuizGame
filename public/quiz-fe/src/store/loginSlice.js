import { createSlice } from '@reduxjs/toolkit';



const saveFromLocalStorage = (data) => {
    localStorage.setItem("user", JSON.stringify(data));
}

export const loginSlice = createSlice({
    name: "userData",
    initialState: [],
    reducers: {
        initUser: (state,action) => {
            
            return [action.payload]
        },

        addUser: (state, action) => {
            saveFromLocalStorage(action.payload)
            return action.payload;
            
        },

        deleteUser: (state, action) => {
            return state.filter((user) => {
                if (user.id !== action.payload) {
                    return user;
                }
            })
        },
    
    }
});

export const { addUser, deleteUser,  initUser } = loginSlice.actions;

export const selectUserData = (state) => state.userData;

export default loginSlice.reducer;